const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

const port = process.env.PORT || 3000;

var app = express();

hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine','hbs');

hbs.registerHelper('getCurrentYear', ()=>{
	return new Date().getFullYear();
});

hbs.registerHelper('screamIt', (text)=>{
	return text.toUpperCase();
});

//MIDDLEWARE EXAMPLE THAT DOES NOTHING EXCEPT MOVE ON THROUGH CALLING NEXT
app.use((req, res, next)=>{
	next();
});

//MIDDLEWARE EXAMPLE THAT LOGS A USER COMING IN
app.use((req, res, next)=>{
	var now = new Date().toString();
	var log = `${now}: ${req.method} ${req.url}`; 
	
	console.log(log);
	
	fs.appendFile('server.log', log + '\n', (err)=>{
		if(err){
			console.log('Unable to write log file with error: ', JSON.stringify(err, undefined, 2));
		}
	});
	
	next();
});

/*
//MIDDLEWARE EXAMPLE DOES NOT EXIT, CALLS MAINTENANCE
app.use((req, res, next)=>{
	res.render('maintenance.hbs', {
		pageTitle: 'Maintenance Example'
	});
});
*/

app.use(express.static(__dirname+'/public'));

app.get('/', (req, res) => {
	res.render('home.hbs', {
		pageTitle: 'Node.js Express Server example',
		welcomeMessage: 'Welcome to the example express server!'
	});
});

app.get('/about', (req, res) => {
	res.render('about.hbs', {
		pageTitle: 'About Page'
	});
});

app.get('/projects', (req, res) => {
	res.render('projects.hbs', {
		pageTitle: 'Projects Page'
	});
});

app.get('/bad', (req, res) => {
	res.send({
		type: 'error',
		message: 'This was an error',
		wentBad: [
			'Badness',
			'Worseness'
		]
	});
});

app.listen(port, ()=>{
	console.log('Server is up on port ', port);
});
